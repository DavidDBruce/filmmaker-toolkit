# Filmmaker Toolkit

Filmmaker Toolkit is a very simple tool for calculating often needed numbers for work in Post Production

## Installation

Currently the application is intended to be sideloaded after compiling in Android Studio.

## Usage

For both calculators, select your input types at the top. Then enter the values you want to calculate. 

* Framerate Calculator
    * Compare two framerates to find the percent speed needed to match the second to the first.
    * Compare a framerate and a percent speed to find the resulting framerate once the speed is applied.
* Aspect Ratio Calculator
    * Find the decimal representation of a width and height
    * Find the missing height or width based on a decimal input.