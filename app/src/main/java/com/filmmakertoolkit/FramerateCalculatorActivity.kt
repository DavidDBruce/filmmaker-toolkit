package com.filmmakertoolkit

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import java.math.BigDecimal
import java.math.RoundingMode

class FramerateCalculatorActivity : AppCompatActivity() {

    private val framerateString = "Framerate"
    private val percentSpeedString = "%Speed"
    private val oneHundred = BigDecimal.valueOf(10000, 2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_framerate_calculator)

        findViewById<Spinner>(R.id.framerateCalculationComponentSPNR1).adapter =
            ArrayAdapter.createFromResource(
                this,
                R.array.framerate_conversion_options,
                android.R.layout.simple_spinner_dropdown_item
            )

        findViewById<Spinner>(R.id.framerateCalculationComponentSPNR2).adapter =
            ArrayAdapter.createFromResource(
                this,
                R.array.framerate_conversion_options,
                android.R.layout.simple_spinner_dropdown_item
            )
    }

    fun determineAndRunFramerateCalculation(view: View) {
        val conversionSelection1 =
            findViewById<Spinner>(R.id.framerateCalculationComponentSPNR1).selectedItem
        val conversionSelection2 =
            findViewById<Spinner>(R.id.framerateCalculationComponentSPNR2).selectedItem

        if (framerateString == conversionSelection1 && framerateString == conversionSelection2) {
            framerateToFramerateCalculation()
        } else if (percentSpeedString == conversionSelection1 && framerateString == conversionSelection2) {
            percentSpeedToFramerate()
        } else if (framerateString == conversionSelection1 && percentSpeedString == conversionSelection2) {
            framerateToPercentSpeed()
        } else {
            Snackbar.make(view, "Calculation not currently supported", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun framerateToFramerateCalculation() {
        val framerate1 =
            BigDecimal.valueOf(
                findViewById<EditText>(R.id.framerateCalculationInputET1).text.toString().toLong()
            )
        val framerate2 = BigDecimal.valueOf(
            findViewById<EditText>(R.id.framerateCalculationInputET2).text.toString().toLong()
        )

        val result = framerate2.multiply(oneHundred).divide(framerate1, BigDecimal.ROUND_HALF_UP)

        setResults(result, percentSpeedString)
    }

    private fun percentSpeedToFramerate() {
        val percentSpeed =
            BigDecimal.valueOf(
                findViewById<EditText>(R.id.framerateCalculationInputET1).text.toString().toLong()
            )
        val framerate = BigDecimal.valueOf(
            findViewById<EditText>(R.id.framerateCalculationInputET2).text.toString().toLong()
        )

        val result = oneHundred.divide(percentSpeed, RoundingMode.HALF_UP).multiply(framerate)

        setResults(result, framerateString)
    }

    private fun framerateToPercentSpeed() {
        val framerate =
            BigDecimal.valueOf(
                findViewById<EditText>(R.id.framerateCalculationInputET1).text.toString().toLong()
            )
        val percentSpeed = BigDecimal.valueOf(
            findViewById<EditText>(R.id.framerateCalculationInputET2).text.toString().toLong()
        )

        val result = percentSpeed.multiply(framerate).divide(oneHundred, RoundingMode.HALF_UP)

        setResults(result, framerateString)
    }

    private fun setResults(result: BigDecimal, type: String) {
        val resultsTV = findViewById<TextView>(R.id.framerateCalculationResultTV)
        if (framerateString == type) {
            resultsTV.text = String.format("%s FPS", result.toString())
        } else if (percentSpeedString == type) {
            try {
                resultsTV.text = String.format("%s%%", result.toString())
            } catch (e: Exception) {
                Log.e("Searcher", "Error writing to resultsTV", e)
            }
        }
    }

}