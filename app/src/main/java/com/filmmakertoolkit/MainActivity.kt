package com.filmmakertoolkit

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun openFramerateCalculator(view: View) {
        val intent = Intent(this, FramerateCalculatorActivity::class.java)
        startActivity(intent)
    }

    fun openAspectRatioCalculator(view: View) {
        val intent = Intent(this, AspectRatioCalculatorActivity::class.java)
        startActivity(intent)
    }

    fun openResolutionCalculator(view: View) {
        val intent = Intent(this, ResolutionCalculatorActivity::class.java)
        startActivity(intent)
    }
}