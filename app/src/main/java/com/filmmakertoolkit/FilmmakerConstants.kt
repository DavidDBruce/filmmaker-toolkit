package com.filmmakertoolkit

import android.view.View
import com.google.android.material.snackbar.Snackbar

class FilmmakerConstants {
    companion object {
        const val WIDTH = "Resolution Width"
        const val HEIGHT = "Resolution Height"
        const val RATIO = "Ratio"
        const val RESOLUTION_CONVERSION_OPTIONS = R.array.resolution_conversion_options

        fun showShortSnackBar(input: String, view: View) {
            Snackbar.make(view, input, Snackbar.LENGTH_SHORT).show()
        }
    }
}