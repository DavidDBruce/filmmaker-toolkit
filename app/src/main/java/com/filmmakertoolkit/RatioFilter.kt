package com.filmmakertoolkit

import android.text.InputFilter
import android.text.Spanned
import java.util.*

class RatioFilter : InputFilter {
    var acceptedChars = "0123456789:."

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {
        return Optional.ofNullable(source)
            .map { string -> string.filter { char -> acceptedChars.contains(char) } }
            .orElse(null)
    }
}