package com.filmmakertoolkit

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import org.apache.commons.lang3.StringUtils
import java.math.BigDecimal
import java.math.BigDecimal.ROUND_DOWN
import java.math.BigDecimal.ROUND_HALF_DOWN
import java.math.BigDecimal.ZERO
import java.math.BigInteger
import java.math.RoundingMode

class AspectRatioCalculatorActivity : AppCompatActivity() {
    private val widthString = "Width"
    private val heightString = "Height"
    private val decimalString = "Decimal"
    private val resultFormat = "%s:%s"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aspect_ratio_calculator)

        findViewById<Spinner>(R.id.aspectRatioCalculationComponentSPNR1).adapter =
            ArrayAdapter.createFromResource(
                this,
                R.array.aspect_ratio_conversion_options,
                android.R.layout.simple_spinner_dropdown_item
            )

        findViewById<Spinner>(R.id.aspectRatioCalculationComponentSPNR2).adapter =
            ArrayAdapter.createFromResource(
                this,
                R.array.aspect_ratio_conversion_options,
                android.R.layout.simple_spinner_dropdown_item
            )
    }

    fun determineAndRunAspectRatioCalculation(view: View) {
        val conversionSelection1 =
            findViewById<Spinner>(R.id.aspectRatioCalculationComponentSPNR1).selectedItem
        val conversionSelection2 =
            findViewById<Spinner>(R.id.aspectRatioCalculationComponentSPNR2).selectedItem

        val input1 = validateAndInterpretUserInput(
            findViewById<EditText>(R.id.aspectRatioCalculationInputET1).text.toString()
        )

        val input2 = validateAndInterpretUserInput(
            findViewById<EditText>(R.id.aspectRatioCalculationInputET2).text.toString()
        )

        if (input1 == null || input2 == null) {
            Snackbar.make(view, "Please complete your input values.", Snackbar.LENGTH_SHORT).show()
        } else if (widthString == conversionSelection1 && heightString == conversionSelection2) {
            widthVsHeight(input1, input2)
        } else if (widthString == conversionSelection1 && decimalString == conversionSelection2) {
            widthVsDecimal(input1.toBigInteger(), input2)
        } else if (heightString == conversionSelection1 && widthString == conversionSelection2) {
            widthVsHeight(input2, input1)
        } else if (heightString == conversionSelection1 && decimalString == conversionSelection2) {
            widthVsDecimal(input1.toBigInteger(), input2)
        } else if (decimalString == conversionSelection1 && widthString == conversionSelection2) {
            widthVsDecimal(input2.toBigInteger(), input1)
        } else if (decimalString == conversionSelection1 && heightString == conversionSelection2) {
            widthVsDecimal(input2.toBigInteger(), input1)
        } else {
            Snackbar.make(view, "Calculation not currently supported", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun validateAndInterpretUserInput(input: String): BigDecimal? {
        if (StringUtils.isNumeric(input.replace(".", ""))) {
            return BigDecimal.valueOf(input.toDouble()).setScale(4, RoundingMode.HALF_DOWN)
        }
        return null
    }

    private fun widthVsHeight(width: BigDecimal, height: BigDecimal) {
        val result = width.divide(height, ROUND_HALF_DOWN)
        setResults(result, ZERO, decimalString)
    }

    private fun widthVsDecimal(width: BigInteger, decimal: BigDecimal) {
        val multiplicationFactor = determineDecimalMultiplicationFactor(decimal)

        val multipliedWidth = width.multiply(BigInteger.valueOf(multiplicationFactor))
        val multipliedDecimal =
            decimal.multiply(BigDecimal.valueOf(multiplicationFactor)).toBigInteger()

        val resultHeight = multipliedWidth.toBigDecimal()
            .divide(multipliedDecimal.toBigDecimal(), RoundingMode.HALF_UP)

        setResults(width.toBigDecimal(), resultHeight, widthString)
    }

    private fun determineDecimalMultiplicationFactor(decimal: BigDecimal): Long {
        var result = 1L
        while (hasDecimalPlaces(BigDecimal.valueOf(result).multiply(decimal))) {
            result *= 10L
        }
        return result
    }

    private fun hasDecimalPlaces(decimal: BigDecimal): Boolean {
        val decimalToTwoDecimals = decimal.setScale(2, ROUND_DOWN)
        val decimalWithZeroDecimals = decimal.setScale(0, ROUND_DOWN)
        if (decimalWithZeroDecimals.compareTo(ZERO) != 0) {
            return decimalToTwoDecimals.remainder(decimalWithZeroDecimals) != ZERO.setScale(2)
        }
        return true
    }

    private fun setResults(result1: BigDecimal, result2: BigDecimal, type: String) {
        val resultsTV = findViewById<TextView>(R.id.aspectRatioCalculationResultTV)
        if ((widthString == type || heightString == type) && (ZERO != result1 && ZERO != result2)) {
            resultsTV.text = String.format(resultFormat, result1.toString(), result2.toString())
        } else if (decimalString == type) {
            try {
                resultsTV.text =
                    String.format(
                        resultFormat,
                        result1.setScale(2, RoundingMode.DOWN).toString(),
                        "1"
                    )
            } catch (e: Exception) {
                Log.e("Searcher", "Error writing to resultsTV", e)
            }
        }
    }
}