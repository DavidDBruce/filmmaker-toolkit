package com.filmmakertoolkit

import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.filmmakertoolkit.FilmmakerConstants.Companion.HEIGHT
import com.filmmakertoolkit.FilmmakerConstants.Companion.RATIO
import com.filmmakertoolkit.FilmmakerConstants.Companion.RESOLUTION_CONVERSION_OPTIONS
import com.filmmakertoolkit.FilmmakerConstants.Companion.WIDTH
import com.filmmakertoolkit.FilmmakerConstants.Companion.showShortSnackBar
import org.apache.commons.lang3.StringUtils
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class ResolutionCalculatorActivity : AppCompatActivity() {
    private lateinit var spinner1: Spinner
    private lateinit var spinner2: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resolution_calculator)

        val resolutionConversionOptions: Array<String> =
            this.resources.getStringArray(RESOLUTION_CONVERSION_OPTIONS)

        initSpinner1(resolutionConversionOptions)
        initSpinner2(resolutionConversionOptions)
    }

    fun determineAndRunResolutionCalculation(view: View) {
        val userResolutionInput1 =
            UserResolutionInput(findViewById(R.id.resolutionCalculationInputET1), spinner1, view)

        val userResolutionInput2 =
            UserResolutionInput(findViewById(R.id.resolutionCalculationInputET2), spinner2, view)

        if (anyAreNull(userResolutionInput1.inputNumber, userResolutionInput2.inputNumber)) {
            showShortSnackBar("Please complete your input values.", view)
        } else {
            determineAndExecuteCalculation(userResolutionInput1, userResolutionInput2, view)
        }
    }

    private fun determineAndExecuteCalculation(
        userResolutionInput1: UserResolutionInput,
        userResolutionInput2: UserResolutionInput,
        view: View
    ) {
        val input1 = userResolutionInput1.inputNumber
        val input2 = userResolutionInput2.inputNumber
        val conversionSelection1 = userResolutionInput1.type
        val conversionSelection2 = userResolutionInput2.type

        if (WIDTH == conversionSelection1 && RATIO == conversionSelection2) {
            widthVsDecimal(input1, input2, view)
        } else if (HEIGHT == conversionSelection1 && RATIO == conversionSelection2) {
            heightVsDecimal(input1, input2, view)
        } else if (RATIO == conversionSelection1 && WIDTH == conversionSelection2) {
            widthVsDecimal(input2, input1, view)
        } else if (RATIO == conversionSelection1 && HEIGHT == conversionSelection2) {
            heightVsDecimal(input2, input1, view)
        } else {
            showShortSnackBar("Calculation not currently supported", view)
        }
    }

    private fun widthVsDecimal(width: BigDecimal, decimal: BigDecimal, view: View) {
        val dividedOutput = width.divide(decimal, RoundingMode.DOWN)
        setResults(width, dividedOutput, view)
    }

    private fun heightVsDecimal(height: BigDecimal, decimal: BigDecimal, view: View) {
        val multipliedOutput = height.multiply(decimal).setScale(0, RoundingMode.HALF_UP)
        setResults(multipliedOutput, height, view)
    }

    private fun setResults(result1: BigDecimal, result2: BigDecimal, view: View) {
        try {
            findViewById<TextView>(R.id.resolutionCalculationResultTV).text = String.format(
                "%sx%s",
                result1.toBigInteger().toString(),
                result2.toBigInteger().toString()
            )
        } catch (e: Exception) {
            showShortSnackBar("Error setting results.", view)
        }
    }

    private fun initSpinner1(resolutionConversionOptions: Array<String>) {
        val editText1 = findViewById<EditText>(R.id.resolutionCalculationInputET1)
        spinner1 = findViewById(R.id.resolutionCalculationComponentSPNR1)
        spinner1.adapter = ArrayAdapter.createFromResource(
            this,
            RESOLUTION_CONVERSION_OPTIONS,
            android.R.layout.simple_spinner_dropdown_item
        )
        spinner1.setSelection(0)
        spinner1.onItemSelectedListener =
            createSpinnerOnItemSelectedListener(resolutionConversionOptions, editText1)
    }

    private fun initSpinner2(resolutionConversionOptions: Array<String>) {
        val editText2 = findViewById<EditText>(R.id.resolutionCalculationInputET2)
        spinner2 = findViewById(R.id.resolutionCalculationComponentSPNR2)
        spinner2.adapter =
            ArrayAdapter.createFromResource(
                this,
                RESOLUTION_CONVERSION_OPTIONS,
                android.R.layout.simple_spinner_dropdown_item
            )
        spinner2.setSelection(2)
        spinner2.onItemSelectedListener =
            createSpinnerOnItemSelectedListener(resolutionConversionOptions, editText2)
    }

    private fun createSpinnerOnItemSelectedListener(
        resolutionConversionOptions: Array<String>,
        editText: EditText
    ): OnItemSelectedListener {
        return object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val conversionSelected = resolutionConversionOptions[position]

                if (isResolutionTypeSelected(conversionSelected)) {
                    setEditTextForResolution(editText)
                } else if (isRatioSelected(conversionSelected)) {
                    setEditTextForRatio(editText)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //Do nothing
            }
        }
    }

    private fun setEditTextForResolution(editText: EditText) {
        if (editTextIsNotNumber(editText)) {
            editText.setText("")
            editText.inputType = InputType.TYPE_CLASS_NUMBER
            editText.hint = "1920"
        }
    }

    private fun setEditTextForRatio(editText: EditText) {
        if (editTextIsNotText(editText)) {
            editText.setText("")
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.hint = "16:9"
            editText.filters = arrayOf(RatioFilter())
        }
    }

    private fun editTextIsNotNumber(editText: EditText): Boolean {
        return editText.inputType != InputType.TYPE_CLASS_NUMBER
    }

    private fun editTextIsNotText(editText: EditText): Boolean {
        return editText.inputType != InputType.TYPE_CLASS_TEXT
    }

    private fun isResolutionTypeSelected(conversionOption: String): Boolean {
        return listOf(HEIGHT, WIDTH).contains(conversionOption)
    }

    private fun isRatioSelected(conversionOption: String): Boolean {
        return RATIO == conversionOption
    }

    private fun anyAreNull(vararg inputs: Any?): Boolean {
        return inputs.toList().stream()
            .anyMatch(Objects::isNull)
    }
}

class UserResolutionInput(editText: EditText, spinner: Spinner, view: View) {
    var type = spinner.selectedItem.toString()
    var inputNumber = validateAndInterpretUserInput(editText.text.toString(), type, view)

    private fun validateAndInterpretUserInput(input: String, type: String, view: View): BigDecimal {
        if (type == RATIO) {
            return validateStringRatioNumeric(input, view)
        } else if ((type == WIDTH || type == HEIGHT)) {
            if (StringUtils.isNumeric(input)) {
                return BigDecimal.valueOf(input.toLong())
            }
            showShortSnackBar("Width or Height must be non decimal numeric.", view)
        }
        return BigDecimal.ONE
    }

    private fun validateStringRatioNumeric(input: String, view: View): BigDecimal {
        if (StringUtils.isNumeric(input.replace(":", "").replace(".", ""))
            && countMatches(input, ":") == 1
            && countMatches(input, ".") < 2
        ) {
            val inputs = input.split(":")
            val numerator = BigDecimal.valueOf(inputs[0].toDouble()).setScale(4, RoundingMode.DOWN)
            val denominator =
                BigDecimal.valueOf(inputs[1].toDouble()).setScale(4, RoundingMode.DOWN)

            return numerator.divide(denominator, 4, RoundingMode.DOWN)
        }
        showShortSnackBar("Invalid ratio input.", view)
        return BigDecimal.ONE
    }

    private fun countMatches(string: String, pattern: String): Int {
        return string.split(pattern)
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()
            .size - 1
    }
}